-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2022 at 05:27 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `backend-uts`
--

-- --------------------------------------------------------

--
-- Table structure for table `agama87`
--

CREATE TABLE `agama87` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_agama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `agama87`
--

INSERT INTO `agama87` (`id`, `nama_agama`, `created_at`, `updated_at`) VALUES
(1, 'Islam', NULL, NULL),
(2, 'Kristen', NULL, NULL),
(3, 'Hindu', NULL, NULL),
(4, 'Budha', NULL, NULL),
(5, 'Konghucu', NULL, NULL),
(6, 'Islam', NULL, NULL),
(7, 'Kristen', NULL, NULL),
(8, 'Hindu', NULL, NULL),
(9, 'Budha', NULL, NULL),
(10, 'Konghucu', NULL, NULL),
(11, 'Islam', NULL, NULL),
(12, 'Kristen', NULL, NULL),
(13, 'Hindu', NULL, NULL),
(14, 'Budha', NULL, NULL),
(15, 'Konghucu', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `detail_data87`
--

CREATE TABLE `detail_data87` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `id_agama` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `foto_ktp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'foto_ktp.png',
  `umur` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_data87`
--

INSERT INTO `detail_data87` (`id`, `id_user`, `alamat`, `tempat_lahir`, `tanggal_lahir`, `id_agama`, `foto_ktp`, `umur`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, NULL, 1, 'foto_ktp.png', NULL, '2022-11-05 05:40:43', '2022-11-05 05:40:43'),
(2, 2, 'surabaya', 'sragen', '2016-06-07', 5, '1667657403.png', 6, '2022-11-05 05:44:28', '2022-11-05 07:10:26'),
(3, 3, 'surakarta', 'jakarta', '2022-11-03', 1, '1667655026.jpg', 0, '2022-11-05 06:27:54', '2022-11-05 07:08:38'),
(4, 4, NULL, NULL, NULL, 1, 'foto_ktp.png', NULL, '2022-11-05 21:08:58', '2022-11-05 21:08:58');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_10_31_064445_detail_data', 1),
(6, '2022_10_31_065106_agama', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users87`
--

CREATE TABLE `users87` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `is_active` tinyint(1) NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'foto.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users87`
--

INSERT INTO `users87` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role`, `is_active`, `foto`) VALUES
(1, 'Naufal', 'Naufal@gmail.com', NULL, '$2y$10$8oeIfIwlPEAbxaDhykCNNuyHCewva22ASBBe2bolrCNtYR9gMu9qe', NULL, '2022-11-05 05:40:43', '2022-11-05 05:40:43', 'admin', 1, 'foto.png'),
(2, 'Franky D Usop', 'Franky88@gmail.com', NULL, '$2y$10$g5mNGFtRp7IgdsTdPG.NZOxI5PyI4aAoH93h4kTyYbd7yvJcgrJ1K', NULL, '2022-11-05 05:44:28', '2022-11-05 08:10:50', 'user', 1, '1667657380.png'),
(3, 'Joni', 'joni@gmail.com', NULL, '$2y$10$lmZ6IyB5NfTI6SwdR5wbtuv7Xclzz3n732WUO/L8LJliHOIMTI33m', NULL, '2022-11-05 06:27:54', '2022-11-05 06:31:27', 'user', 1, '1667655019.jpg'),
(4, 'Paijo Salaman', 'Paijo@gmail.com', NULL, '$2y$10$rJRF0eM7MdUyzVAEf8bGM.ipp8dUXjfGCMV/3ywNerKRC2Ldj.tES', NULL, '2022-11-05 21:08:58', '2022-11-05 21:08:58', 'admin', 1, 'foto.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agama87`
--
ALTER TABLE `agama87`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_data87`
--
ALTER TABLE `detail_data87`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users87`
--
ALTER TABLE `users87`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users87_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agama87`
--
ALTER TABLE `agama87`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `detail_data87`
--
ALTER TABLE `detail_data87`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users87`
--
ALTER TABLE `users87`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
